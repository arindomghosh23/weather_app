import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whether_bloc_app/bloc/weather_bloc.dart';
import 'package:whether_bloc_app/model/weather.dart';
import 'package:whether_bloc_app/widgets/common_widgets.dart';

class WeatherDetailsScreen extends StatefulWidget {
  final Weather masterWeather;

  WeatherDetailsScreen({
    @required this.masterWeather,
  });

  @override
  State<StatefulWidget> createState() {
    return WeatherDetailsScreenState();
  }
}

class WeatherDetailsScreenState extends State<WeatherDetailsScreen> {
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    BlocProvider.of<WeatherBloc>(context)
      ..add(GetWeatherDetail(widget.masterWeather));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weather Details"),
      ),
      body: BlocListener<WeatherBloc, WeatherState>(
        listener: (context, state) {
          if (state is WeatherLoaded) {
            print('loaded ${state.weather.cityName}');
          }
        },
        child: BlocBuilder<WeatherBloc, WeatherState>(
          builder: (BuildContext context, WeatherState state) {
            if (state is WeatherLoading) {
              return buildLoading();
            } else if (state is WeatherLoaded) {
              return buildColumnWithData(state.weather);
            }
          },
        ),
      ),
    );
  }

  Widget buildColumnWithData(Weather weather) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            weather.cityName,
            style: TextStyle(
              fontSize: 40,
              fontWeight: FontWeight.w700,
            ),
          ),
          Text(
            "${weather.temperature.toStringAsFixed(1)} °C",
            style: TextStyle(fontSize: 80),
          ),
          Text(
            "${weather.temperatureFahrenheit?.toStringAsFixed(1)} °F",
            style: TextStyle(fontSize: 80),
          ),
        ],
      ),
    );
  }
}
