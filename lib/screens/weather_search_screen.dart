import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:whether_bloc_app/bloc/weather_bloc.dart';
import 'package:whether_bloc_app/model/weather.dart';
import 'package:whether_bloc_app/router.gr.dart';
import 'package:whether_bloc_app/widgets/city_input_field.dart';
import 'package:whether_bloc_app/widgets/common_widgets.dart';

class WeatherSearchScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WeatherSearchScreenState();
  }
}

class WeatherSearchScreenState extends State<WeatherSearchScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fake Weather App"),
      ),
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: BlocListener<WeatherBloc, WeatherState>(
          listener: (context, state) {
            if (state is WeatherError) {
              Scaffold.of(context).showSnackBar(SnackBar(
                content: Text(state.message),
              ));
            }
          },
          child: BlocBuilder<WeatherBloc, WeatherState>(
            builder: (BuildContext context, WeatherState state) {
              if (state is WeatherInitial) {
                return buildInitialInput(CityInputField(
                  onSubmit: _submitWeather,
                ));
              } else if (state is WeatherLoading) {
                return buildLoading();
              } else if (state is WeatherLoaded) {
                return buildColumnWithData(state.weather);
              } else if (state is WeatherError) {
                return buildInitialInput(CityInputField(
                  onSubmit: _submitWeather,
                ));
              }
            },
          ),
        ),
      ),
    );
  }

  Widget buildColumnWithData(Weather weather) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          weather.cityName,
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          "${weather.temperature.toStringAsFixed(1)} °C",
          style: TextStyle(fontSize: 80),
        ),
        CityInputField(
          onSubmit: _submitWeather,
        ),
        RaisedButton(
          child: Text('See Details'),
          color: Colors.lightBlue[100],
          onPressed: () {
            ExtendedNavigator.root.pushNamed(
              Routes.weatherDetailsScreen,
              arguments: WeatherDetailsScreenArguments(masterWeather: weather),
            );
          },
        )
      ],
    );
  }

  void _submitWeather(String name) {
    final weatherBloc = BlocProvider.of<WeatherBloc>(context);
    weatherBloc.add(GetWeather(name));
  }
}
