import 'dart:math';

import 'package:whether_bloc_app/model/weather.dart';
import 'package:whether_bloc_app/repository/weather_repository.dart';

class WeatherRepositoryImpl extends WeatherRepository {
  @override
  Future<Weather> fetchWeather(String cityName) {
    return Future.delayed(Duration(seconds: 3), () {
      final random = Random();
      if (random.nextBool()) {
        throw NetworkError();
      }
      return Weather(
        cityName: cityName,
        temperature: 20 + Random().nextInt(15) + random.nextDouble(),
      );
    });
  }

  @override
  Future<Weather> fetchDetailedWeather(Weather weather) {
    return Future.delayed(Duration(seconds: 3), () {
      return Weather(
        cityName: weather.cityName,
        temperature: weather.temperature,
        temperatureFahrenheit: weather.temperature * 1.8 + 32,
      );
    });
  }
}

class NetworkError extends Error {}
