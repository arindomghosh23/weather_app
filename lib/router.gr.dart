// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:whether_bloc_app/screens/weather_search_screen.dart';
import 'package:whether_bloc_app/screens/weather_details_screen.dart';
import 'package:whether_bloc_app/model/weather.dart';

class Routes {
  static const String weatherSearchScreen = '/';
  static const String weatherDetailsScreen = '/weather-details-screen';
  static const all = <String>{
    weatherSearchScreen,
    weatherDetailsScreen,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.weatherSearchScreen, page: WeatherSearchScreen),
    RouteDef(Routes.weatherDetailsScreen, page: WeatherDetailsScreen),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    WeatherSearchScreen: (RouteData data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => WeatherSearchScreen(),
        settings: data,
      );
    },
    WeatherDetailsScreen: (RouteData data) {
      var args = data.getArgs<WeatherDetailsScreenArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) =>
            WeatherDetailsScreen(masterWeather: args.masterWeather),
        settings: data,
      );
    },
  };
}

// *************************************************************************
// Arguments holder classes
// **************************************************************************

//WeatherDetailsScreen arguments holder class
class WeatherDetailsScreenArguments {
  final Weather masterWeather;
  WeatherDetailsScreenArguments({@required this.masterWeather});
}
