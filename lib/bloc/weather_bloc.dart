import 'dart:async';

import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:whether_bloc_app/model/weather.dart';
import 'package:whether_bloc_app/repository/weather_repository.dart';

part 'weather_event.dart';

part 'weather_state.dart';

class WeatherBloc extends HydratedBloc<WeatherEvent, WeatherState> {
  final WeatherRepository _repository;

  WeatherBloc(this._repository) : super(WeatherInitial());

  @override
  Stream<WeatherState> mapEventToState(
    WeatherEvent event,
  ) async* {
    if (event is GetWeather) {
      yield WeatherLoading();
      try {
        final weather = await _repository.fetchWeather(event.cityName);
        yield WeatherLoaded(weather);
      } on Error {
        yield WeatherError("Couldn't fetch weather data for ${event.cityName}");
      }
    } else if (event is GetWeatherDetail) {
      yield WeatherLoading();
      final weather = await _repository.fetchDetailedWeather(event.weather);
      yield WeatherLoaded(weather);
    }
  }

  @override
  WeatherState fromJson(Map<String, dynamic> json) {
    return WeatherLoaded(Weather.fromJson(json));
  }

  @override
  Map<String, dynamic> toJson(WeatherState state) {
    if (state is WeatherLoaded) {
      return state.weather.toJson();
    } else {
      return null;
    }
  }
}
