part of 'weather_bloc.dart';

abstract class WeatherEvent extends Equatable {
  const WeatherEvent([List props = const []]);
}

class GetWeather extends WeatherEvent {
  final String cityName;

  GetWeather(this.cityName) : super([cityName]);

  @override
  List<Object> get props => [cityName];

  @override
  bool get stringify => false;
}

class GetWeatherDetail extends WeatherEvent {
  final Weather weather;

  GetWeatherDetail(this.weather) : super([weather]);

  @override
  List<Object> get props => [weather];
}
