

import 'package:auto_route/auto_route_annotations.dart';
import 'package:whether_bloc_app/screens/weather_details_screen.dart';
import 'package:whether_bloc_app/screens/weather_search_screen.dart';

@MaterialAutoRouter(
  routes: [
    MaterialRoute(page: WeatherSearchScreen, initial: true),
    MaterialRoute(page: WeatherDetailsScreen)
  ]
)
class $Router{}