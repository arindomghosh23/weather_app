# weather_bloc_app

Sample project to showcase
- [ ] -bloc pattern
- [ ] -Hydrated bloc
- [ ] -auto_route
- [ ] -equatable

This is a basic application which shows  the implementation of Bloc pattern
using flutter_bloc. Hyderated bloc to retain last user status when the app is closed auto_route for navigation.

![Screenshot_1594300918](/uploads/faecb5a9f5deaca2ea61515a25788bf5/Screenshot_1594300918.png)

![Screenshot_1594300925](/uploads/0529c509a2ba14b4414395856a7bbf12/Screenshot_1594300925.png)